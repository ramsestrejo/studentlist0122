
package student;

import static org.junit.Assert.*;

//this is a test 2

	public class Student {
	private String name;
	private String id;
	public Student(String n, String i) {
		name = n;
		id = i;
	}
	public Student () {
		name = null;
		id=null;
	}
	public String getName() {
		return name;
	}
	public String getId() {
		return id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setId(String id) {
		this.id = id;
	}
}